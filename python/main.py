import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        #for a in data:
        #    print a['piecePosition']
        self.pieceIndex=data[0]['piecePosition']['pieceIndex']
        #print data
        self.angel=data[0]['angle']
        self.inPieceDistance=data[0]['piecePosition']['inPieceDistance']
        #print self.count_TrackPieces
        #print self.pieceIndex
        #print self.piece_info[self.pieceIndex]
        #print self.piece_info
        if self.pieceIndex <=self.count_TrackPieces-2:
            if 'angle' in self.piece_info[self.pieceIndex] and 'angle' in self.piece_info[self.pieceIndex+1]:
                self.throttle(0.5)
                #print str(self.inPieceDistance) + " From angle"
            elif 'angle' in self.piece_info[self.pieceIndex] and 'angle' not in self.piece_info[self.pieceIndex+1]:
                self.throttle(0.6)
            elif 'angle' not in self.piece_info[self.pieceIndex] and 'angle' in self.piece_info[self.pieceIndex+1]:
                self.throttle(0.7)
            else:
                self.throttle(0.8)

        else:
            self.throttle(0.7)
                #print self.inPieceDistance
            #print self.inPieceDistance  
        #if self.inPieceDistance < 90.00:
        #    print self.inPieceDistance
        #    self.throttle(0.9)
        #else:
        #    print str(self.inPieceDistance) + " from"
        #    self.throttle(0.56)

    def on_game_init(self,data):
        #print data
        self.piece_info=[]
        self.count_TrackPieces=0
        self.pieces_length=data['race']['track']['pieces'][0]['length']
        self.length=data['race']['track']['pieces'][1]['length']
        #self.pieces_switch=data['race']['track']['pieces'][1]['switch']
        #self.pieces_radius=data['race']['track']['pieces'][2]['radius']
        #self.pieces_angle=data['race']['track']['pieces'][2]['angle']
        self.lanes_distFCentre=data['race']['track']['lanes'][0]['distanceFromCenter']
        self.lanes_index=data['race']['track']['lanes'][0]['index']
        self.startingPointX=data['race']['track']['startingPoint']['position']['x']
        self.startingPointY=data['race']['track']['startingPoint']['position']['y']
        self.stratingPointAngle=data['race']['track']['startingPoint']['angle']
        #print self.length
        for a in data['race']['track']['pieces']:
            #print a
            self.piece_info.append(a)
            self.count_TrackPieces+=1
            #if 'angle' in a:
            #    print a
        #print self.count_TrackPieces
        #print self.piece_info

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
        
    def switch_lane(self,data):
        self.msg("Left", switchLane)
        self.ping()    

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit' : self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            #print ("Testing Data {0}:".format(data))
            #print ("Test {0}".format(data['name']))
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
